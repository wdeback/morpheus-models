# Tutorials

Models used in workshops and tutorials are posted here. 

## Graphical user interface

## Intracellular ODE models

### Lotka-Volterra Predator Prey

- Non-spatial
- Global Variables
- System / DiffEqn
- Analysis Logger
  - Time plot
  - Phase plot
  
### Toggle switch

- Non-spatial
- CellType / Property
- System / DiffEqn
- Analysis Logger
  - Time plot
  - Phase plot

- Multiple cells: Population / CellLattice

### 
