_Note: These models are not yet encoded in MorpheusML v2 and will yield Fixboard errors upon loading._

# Maze challenge

How can a cell solve a maze?

Inspired by: 

Scherber et al., [Epithelial cell guidance by self-generated EGF gradients](http://dx.doi.org/10.1039/C2IB00106C), _Integrative Biology_, 
2012.

https://vimeo.com/110488326


## Challenge 1

- With an predefined gradient.

## Challenge 2

- Without a predefined gradient.
