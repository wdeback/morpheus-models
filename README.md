# Morpheus models collection

This is a collection of simulation models for [Morpheus](https://imc.zih.tu-dresden.de/wiki/morpheus). 
This collection extends the example models that are included in Morpheus 2.

Models are encoded in MorpheusML, a domain-specific XML format for 
storing and sharing multi-scale and multicellular simulation models.

All models are encoded in MorpheusML version 2, unless specified otherwise. 
